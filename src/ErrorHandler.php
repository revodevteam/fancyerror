<?php
namespace Framework\FancyError;

use Exception;
use ErrorException;

class ErrorHandler
{

    const ERROR_TYPES = [
        E_ERROR => 'FATAL_ERROR',
        E_WARNING => 'ERROR_WARNING',
        E_PARSE => 'ERROR_PARSE',
        E_NOTICE => 'ERROR_NOTICE'
    ];

    public function __construct()
    {
        set_error_handler(array($this, 'errorHandler'));
        set_exception_handler(array($this, 'exceptionHandler'));
    }

    /**
     * Turn Internal errors into Exception
     * @param type $level
     * @param type $message
     * @param type $file
     * @param type $line
     * @throws ErrorException
     */
    public function errorHandler($level, $message, $file, $line)
    {
        throw new ErrorException($message, 404, $level, $file, $line);

        exit(1);
    }

    /**
     * Handle any errors
     * @param Exception $exception
     */
    public function exceptionHandler($exception)
    {
        $errorCode = $exception->getCode() ?: 404;

        http_response_code($errorCode);

        if (isDebug() === true) {
            $errorPack = [];
            $errorPack['error'] = $exception->getMessage();
            $errorPack['file'] = str_replace(ROOT_DIR, null, $exception->getFile());
            $errorPack['line'] = $exception->getLine();
            $errorPack['error_code'] = $errorCode;
            $errorPack['stacktrace'] = $this->getTraceString($exception->getTrace());
            $errorPack['previous'] = $exception->getPrevious();
            $errorPack['codebase'] = $this->getCodeBase($exception->getFile(), $exception->getLine());

            if ($exception instanceof ErrorException) {
                $errorPack['reporter'] = 'PHP Parser';
                $errorPack['error_type'] = $this->errorType($exception->getSeverity());
            } else {
                $errorPack['reporter'] = 'Framework Internal';
                $errorPack['error_type'] = $this->errorType($errorCode);
            }

            $template = file_get_contents(__DIR__ . DS . 'DisplayTemplate.php');

            if (!empty($errorPack)) {
                foreach ($errorPack as $name => $data) {
                    $template = str_replace('#' . strtoupper($name) . '#', $data, $template);
                }
            }

            echo $template;
        } else {
            $errorLogger = new ErrorLogger();
            $errorText = DATETIME . ' # ' . $exception->getMessage() . ' Thrown in ' . $exception->getFile() . ' on line ' . $exception->getLine();
            $errorLogger->log($errorText, $exception->getCode());

            $this->showErrorTemplate($exception);
        }

        exit(1);
    }

    protected function showErrorTemplate($exception)
    {
        $acceptable = explode(',', $_SERVER['HTTP_ACCEPT'] ?? 'text/html');

        if (in_array('text/html', $acceptable, true)) {
            // We can show error page as HTML
            $templateClass = 'App' . NS . 'Templates' . NS . TEMPLATE_SITE . NS . 'TemplateHandler';

            if (session_id() === '') {
                session_start();
            }

            $templateHandler = new $templateClass(APP_DIR . DS . 'Views' . DS . 'Errors' . DS . ($exception->getCode() ?: 404) . '.php');

            if (method_exists($templateHandler, 'preRender')) {
                $templateHandler->preRender();
            }
            if (method_exists($templateHandler, 'render')) {
                $templateHandler->render();
            }
            if (method_exists($templateHandler, 'postRender')) {
                $templateHandler->postRender();
            }
        } else {
            // We are gonna force Json here
            header('Content-Type: application/json');
            echo json_encode(['error_no' => ($exception->getCode() ?: 404),
                'error_msg' => 'oops! something went wrong!']);
        }
    }

    public function errorType($errorCode)
    {
        if (isset(self::ERROR_TYPES[$errorCode])) {
            return str_replace('_', ' ', self::ERROR_TYPES[$errorCode]);
        }

        return 'FATAL ERROR';
    }

    public function getTraceString($traceStack)
    {
        $stringTrace = '';
        $totalTrace = count($traceStack);

        if (!empty($traceStack)) {
            foreach ($traceStack as $trace) {
                $trace = (object) $trace;

                $stringTrace .= '<div>#' . $totalTrace . ' ' . (isset($trace->file) ? $trace->file : '') . '(<span class="spn-green">' . (isset($trace->line) ? $trace->line : '') . ')</span>';

                if (isset($trace->class) && strpos($trace->class, 'Framework\OS\FancyError\ErrorHandler') === false) {
                    $stringTrace .= ' : ' . $trace->class . '-><span class="trace-func">' . $trace->function . '(';
                    if (!empty($trace->args)) {
                        $args = [];
                        foreach ($trace->args as $arg) {
                            if (is_object($arg)) {
                                $args[] = '(' . gettype($arg) . ') ' . get_class($arg);
                            } else {
                                $args[] = '(' . gettype($arg) . ') ' . json_encode($arg);
                            }
                        }
                        $stringTrace .= '<span class="hide hide-seek">' . preg_replace('#([\"|\'])([\s\S]+?)([\"|\'])#', '$1<b>$2</b>$3', implode(', ', $args)) . '</span>';
                    }
                    $stringTrace .= ')<span>';
                }

                $stringTrace = $this->prettyTrace($stringTrace);
                $stringTrace .= '</div>';
                $totalTrace--;
            }
        }

        return $stringTrace;
    }

    public function prettyTrace($trace)
    {
        $trace = str_replace(ROOT_DIR, null, $trace);

        return $trace;
    }

    public function getCodeBase($file, $line)
    {
        if (!file_exists($file)) {
            return false;
        }

        $code = file_get_contents($file);
        $codeLines = explode(PHP_EOL, $code);
        $codeBase = '';
        $blockStart = $line - 7;
        $blockEnd = $line + 4;

        for ($i = $blockStart; $i < $blockEnd; $i++) {
            if (isset($codeLines[$i])) {
                $codeLine = $codeLines[$i];
                $ln = $i + 1;

                if ($line !== ($i + 1)) {
                    // String
                    $codeLine = preg_replace('#([\'|\"]+)(.*?)([\'|\"]+)#', '$1<font class="text">$2</font>$3', $codeLine);

                    // Integer
                    $codeLine = preg_replace('#(\s)([0-9]+)([\s|\;])#', '$1<font color="orangered">$2</font>$3', $codeLine);

                    // Local Variables
                    $codeLine = preg_replace('#\$([a-zA-Z0-9]+)#', '<font color="brown">\$$1</font>', $codeLine);

                    // Lets blue all definations
                    $codeLine = preg_replace('#(\s)([a-zA-Z0-9]+)(\s)#', '$1<font color="blue">$2</font>$3', $codeLine);
                    $codeLine = preg_replace('#(\s)([a-zA-Z0-9]+)(\s)#', '$1<font color="blue">$2</font>$3', $codeLine);
                    $codeLine = preg_replace('#(\s)([a-zA-Z0-9]+)(\s)#', '$1<font color="blue">$2</font>$3', $codeLine);

                    // Object properties
                    $codeLine = preg_replace('#->([a-zA-Z0-9]+)([\s|\)|\;]+)#', '-><font color="green">$1</font>$2', $codeLine);

                    // method names
                    $codeLine = preg_replace('#([a-zA-Z0-9]+)\(#', '<b>$1</b>(', $codeLine);
                }

                $codeLine = preg_replace('#//(.*)$#', '<span class="commentLine">//$1</span>', $codeLine);
                if (strpos($codeLine, '/*') !== false) {
                    $codeLine = preg_replace('#/\*(.*)$#', '<span class="commentBlock">/*$1', $codeLine);
                    $commentBlock = true;
                }
                if (strpos($codeLine, '*/') !== false && !empty($commentBlock)) {
                    $codeLine = preg_replace('#^(.*)\*/#', '$1*/</span>', $codeLine);
                    $commentBlock = false;
                }


                if ($line === ($i + 1)) {
                    $prefix = '<span class="activeLine">';
                    $suffix = '</span>';
                    $seperator = '';
                } else {
                    $prefix = '';
                    $suffix = '';
                    $seperator = PHP_EOL;
                }
                $codeBase .= $prefix . $ln . ".\t" . $codeLine . $suffix . $seperator;
            }
        }

        return $codeBase;
    }
}
