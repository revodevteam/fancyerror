<?php

namespace Framework\FancyError;

use Framework\FileSystem\FileManager;

class ErrorLogger extends FileManager
{

    const errorDIR = ROOT_DIR . DS . 'var' . DS . 'errors';

    public function __construct()
    {
        parent::__construct();

        $this->checkDIR(self::errorDIR);
    }

    public function log($error, $code)
    {
        $errorFile = self::errorDIR . DS . 'error_log';

        $this->appendToFile($error, $errorFile);
    }

}
