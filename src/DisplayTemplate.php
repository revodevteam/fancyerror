
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
            .activeLine {
                color:white;
                font-weight: bold;
                background-color:cornflowerblue;
                display:block;
            }

            .commentLine {
                color:grey;
            }

            .commentBlock {
                color:grey;
            }

            .spn-green {
                color: green;
                font-weight: bold;
            }

            .hide-seek {
                color: royalblue;
            }

            .text {
                color: olive;
            }
            
            .text font {
                color: inherit;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h2><font color="green">#ERROR#</font></h2>
                <p> Thrown in <b>#FILE#</b> on line <font color="red"><b>#LINE#</b></font></p>
                <p><pre>#CODEBASE#</pre></p>
                <p>Type : #ERROR_TYPE#</p>
                <p>Thrown by: #REPORTER#</p>

                <p>Stack Trace : <pre>#STACKTRACE#</pre></p>
            <a target="_blank" href="https://google.com/search?q=#ERROR#" class="btn btn-default"><b>Search on Google</b></a>
                <footer>
                    <!-- Latest compiled and minified JavaScript -->
                    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                </footer>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("span.trace-func").click(function () {
                    $(this).children(".hide-seek").toggleClass("hide");
                });
            });
        </script>
    </body>
</html>